import os
import sys

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("usage: python list_files.py <directory>")
        exit(0)
    dir = sys.argv[1]
    if not os.path.isdir(dir):
        print("invalid directory")
        exit(0)
    data = list(filter(lambda d: "left" in d or "right" in d and os.path.isdir(d), [x[0] for x in os.walk(dir)]))

    for item in data:
        last = item.split("/")[-1]
        files = []
        for x in range(0, len(os.listdir(item))):
            files.append("%s/%s_%s.jpg" % (item, last, x))
        print(files)