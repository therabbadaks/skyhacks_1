import glob
import cv2
import keras
import tensorflow as tf
import numpy as np
from keras import *
from keras.layers import *

if __name__ == '__main__':
    class_names = ['End Train', 'Start Train', 'Transition', 'Wagon']
    # model = keras.Sequential([
    #     keras.layers.Flatten(input_shape=(1024, 1280)),
    #     keras.layers.Flatten(input_shape=(1024, 1280, )),
    #     keras.layers.Dense(20, activation=tf.nn.relu),
    #     keras.layers.Dense(4, activation=tf.nn.softmax)
    # ])
    model = keras.Sequential()
    model.add(Flatten())
    model.add(Dense(2, activation='softmax'))
    model.add(Dense(4, activation='softmax'))

    model.compile(optimizer=tf.train.AdamOptimizer(),
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    train_data_endTrain = glob.glob('classified/0_0_left_selected/endTrain/*.jpg')
    train_data_startTrain = glob.glob('classified/0_0_left_selected/startTrain/*.jpg')
    train_data_transition = glob.glob('classified/0_0_left_selected/transition/*.jpg')
    train_data_wagon = glob.glob('classified/0_0_left_selected/wagon/*.jpg')
    train_data = []
    train_labels = []
    for item in train_data_endTrain:
        train_data.append(cv2.imread(item))
        train_labels.append(0)
    for item in train_data_startTrain:
        train_data.append(cv2.imread(item))
        train_labels.append(1)
    for item in train_data_transition:
        train_data.append(cv2.imread(item))
        train_labels.append(2)
    for item in train_data_wagon:
        train_data.append(cv2.imread(item))
        train_labels.append(3)

    test_data_endtest = glob.glob('classified/0_1_left_selected/endtest/*.jpg')
    test_data_starttest = glob.glob('classified/0_1_left_selected/starttest/*.jpg')
    test_data_transition = glob.glob('classified/0_1_left_selected/transition/*.jpg')
    test_data_wagon = glob.glob('classified/0_1_left_selected/wagon/*.jpg')

    test_data = []
    test_labels = []
    for item in test_data_endtest:
        test_data.append(cv2.imread(item))
        test_labels.append(0)
    for item in test_data_starttest:
        test_data.append(cv2.imread(item))
        test_labels.append(1)
    for item in test_data_transition:
        test_data.append(cv2.imread(item))
        test_labels.append(2)
    for item in test_data_wagon:
        test_data.append(cv2.imread(item))
        test_labels.append(3)

    train_data = np.array(train_data)
    train_labels = np.array(train_labels)
    test_data = np.array(test_data)
    test_labels = np.array(test_labels)

    train_data = train_data.astype('float32')
    test_data = test_data.astype('float32')

    train_data /= 255
    test_data /= 255

    model.fit(train_data, train_labels, epochs=5)

    scores = model.evaluate(test_data, test_labels)

    for file in glob.glob('../data/Validation/0_60/0_60_left/*.jpg'):
        predicate = model.predict(np.array([cv2.imread(file)]))
        label_index = np.argmax(predicate)
        print(label_index)
        print(file)
        print()