from functools import reduce
import cv2
import glob

if __name__ == '__main__':
    wagons = []
    transitions = []

    for file in glob.glob('classified/**/**/*.jpg')[0:100]:
        image = cv2.imread(file)
        height, width = image.shape[:2]
        left = image[0:height, 0:int(width/3)]
        center = image[0:height, int(width/3):int(width/3*2)]
        right = image[0:height, int(width/3*2):width]

        hist1 = cv2.calcHist([left], [0], None, [256], [0, 256])
        hist2 = cv2.calcHist([center], [0], None, [256], [0, 256])
        hist3 = cv2.calcHist([right], [0], None, [256], [0, 256])

        whites = [hist1[200:256], hist2[200:256],  hist3[200:256]]
        average = [
            reduce(lambda x, y: x + y, whites[0]) / len(whites[0]),
            reduce(lambda x, y: x + y, whites[1]) / len(whites[1]),
            reduce(lambda x, y: x + y, whites[2]) / len(whites[2])
        ]
        average.sort()
        maximal = [max(hist1), max(hist2), max(hist3)]
        maximal.sort()
        if maximal[1] > maximal[0] * 1.7 or maximal[2] > maximal[1] * 1.7 or average[0] > average[1] * 1.7 or average[1] > average[2] * 1.7:
            transitions.append(file)
        else:
            wagons.append(file)

    print("wagons:")
    print(wagons)
    print("transitions:")
    print(transitions)

