import cv2
import numpy as np
import pytesseract
from PIL import Image
from pytesseract import image_to_string

if __name__ == "__main__":
    # Path of working folder on Disk
    src_path = "img/"

    def get_string(img_path):
        # Read image with opencv
        img = cv2.imread(img_path)

        # Convert to gray
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Apply dilation and erosion to remove some noise
        kernel = np.ones((1, 1), np.uint8)
        img = cv2.dilate(img, kernel, iterations=1)
        img = cv2.erode(img, kernel, iterations=1)


        # Recognize text with tesseract for python
        result = pytesseract.image_to_string(img)

        # Remove template file
        # os.remove(temp)

        return result

    print('--- Start recognize text from image ---')
    print(get_string(src_path + "cont.jpg"))

    print("------ Done -------")